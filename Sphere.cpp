//============================================================
// STUDENT NAME: Kwan Yong Kang Nicholas
// MATRIC NO.  : A0080933
// NUS EMAIL   : a0080933@u.nus.edu
// COMMENTS TO GRADER:
// <comments to grader, if any>
//
// ============================================================
//
// FILE: Sphere.cpp



#include <cmath>
#include "Sphere.h"

using namespace std;



bool Sphere::hit( const Ray &r, double tmin, double tmax, SurfaceHitRecord &rec ) const 
{
	//***********************************************
    //*********** WRITE YOUR CODE HERE **************
    //***********************************************

	float alpha = 1.0f;
	Vector3d translated = r.origin() - center;
	float beta = 2 * dot(r.direction(), translated);
	float gamma = dot(translated, translated) - radius * radius;

	float d = beta*beta - 4 * alpha * gamma; // discriminant
	if (d < 0) {
		return false; 
	}
	float t1 = (-beta + sqrt(d)) / (2 * alpha);
	float t2 = (-beta - sqrt(d)) / (2 * alpha);

	float t = 0;
	
	if (t1 > 0 && t1 < t2) {
		t = t1;
	}
	else if (t2 > 0) {
		t = t2;
	}

	if (t >= tmin && t <= tmax)
	{
		// We have a hit -- populat hit record. 
		rec.t = t;
		rec.p = r.pointAtParam(t);
		rec.normal = rec.p - center;//alpha * n0 + beta * n1 + gamma * n2;
		rec.mat_ptr = matp;
		return true;
	}

	return false; // You can remove/change this if needed.
}




bool Sphere::shadowHit( const Ray &r, double tmin, double tmax ) const 
{
	//***********************************************
    //*********** WRITE YOUR CODE HERE **************
    //***********************************************

	float alpha = 1.0f;
	Vector3d translated = r.origin() - center;
	float beta = 2 * dot(r.direction(), translated);
	float gamma = dot(translated, translated) - radius * radius;

	float d = beta*beta - 4 * alpha * gamma; // discriminant
	if (d < 0) {
		return false;
	}
	float t1 = (-beta + sqrt(d)) / (2 * alpha);
	float t2 = (-beta - sqrt(d)) / (2 * alpha);

	float t = 0;

	if (t1 > 0 && t1 < t2) {
		t = t1;
	}
	else if (t2 > 0) {
		t = t2;
	}

	return (t >= tmin && t <= tmax);
}

